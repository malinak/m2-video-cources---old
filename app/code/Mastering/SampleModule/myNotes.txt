- in cli to make visible parameters for xml for phpStorm
bin/magento dev:urn-catalog:generate .idea/misc.xml

- enable module
bin/magento module:enable Mastering_SampleModule

- finalize module registration (update module version in db)
bin/magento setup:upgrade

- table contain module version
setup_module

- when xml files are changed do
bin/magento cache:flush

- run all cron commands
bin/magento cron:run