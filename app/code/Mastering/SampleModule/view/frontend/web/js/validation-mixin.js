define(function () {
    'use strict';

    var extension = {
        isValid: function () { //override isValid method for extension script (validation.js in this tutorial)
            return true;
        }
    };

    return function (target) {
        return target.extend(extension);
    }
});