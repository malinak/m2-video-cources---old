var config = {
    'map': { // map is to replace whole file
        '*': { //for which modules to make override. * - for all
            'mage/validation': 'Mastering_SampleModule/js/validation' //lib/web/mage/validation.js
        }
    },
    config: { //to use mixin
        mixins: { // mixins is to rewrite any method of js components
            'Mastering_SampleModule/js/validation': { //specify the component we want to override
                'Mastering_SampleModule/js/validation-mixin': true //set value for mixin
            }
        }
    }
};